package com.selegant.logsdemo.controller;

import com.selegant.logsdemo.config.annotion.BussinessLog;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author selegant
 */
@RestController
public class DemoController {

    @GetMapping("demo")
    public String demoAction(){
        return "demo";
    }


    @BussinessLog(value = "测试",key = "test")
    @GetMapping("demo2")
    public String demoAction2(){
        return "demo";
    }
}
