package com.selegant.logsdemo.core.common.constant.dictmap.factory;

import com.selegant.logsdemo.core.common.constant.factory.IConstantFactory;
import com.selegant.logsdemo.core.util.SpringContextHolder;
import com.selegant.logsdemo.dao.DictMapper;
import com.selegant.logsdemo.model.Dict;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 常量的生产工厂
 *
 * @author fengshuonan
 * @date 2017年2月13日 下午10:55:21
 */
@Component
@DependsOn("springContextHolder")
public class ConstantFactory implements IConstantFactory {

    private DictMapper dictMapper = SpringContextHolder.getBean(DictMapper.class);

    public static IConstantFactory me() {
        return SpringContextHolder.getBean("constantFactory");
    }


    /**
     * 根据用户id获取用户名称
     *
     * @param userId
     * @author stylefeng
     * @Date 2017/5/9 23:41
     */
    @Override
    public String getUserNameById(Integer userId) {
        return null;
    }

    /**
     * 根据用户id获取用户账号
     *
     * @param userId
     * @author stylefeng
     * @date 2017年5月16日21:55:371
     */
    @Override
    public String getUserAccountById(Integer userId) {
        return null;
    }

    /**
     * 通过角色ids获取角色名称
     *
     * @param roleIds
     */
    @Override
    public String getRoleName(String roleIds) {
        return null;
    }

    /**
     * 通过角色id获取角色名称
     *
     * @param roleId
     */
    @Override
    public String getSingleRoleName(Integer roleId) {
        return null;
    }

    /**
     * 通过角色id获取角色英文名称
     *
     * @param roleId
     */
    @Override
    public String getSingleRoleTip(Integer roleId) {
        return null;
    }

    /**
     * 获取部门名称
     *
     * @param deptId
     */
    @Override
    public String getDeptName(Integer deptId) {
        return null;
    }

    /**
     * 获取菜单的名称们(多个)
     *
     * @param menuIds
     */
    @Override
    public String getMenuNames(String menuIds) {
        return null;
    }

    /**
     * 获取菜单名称
     *
     * @param menuId
     */
    @Override
    public String getMenuName(Long menuId) {
        return null;
    }

    /**
     * 获取菜单名称通过编号
     *
     * @param code
     */
    @Override
    public String getMenuNameByCode(String code) {
        return null;
    }

    /**
     * 获取字典名称
     *
     * @param dictId
     */
    @Override
    public String getDictName(Integer dictId) {
        return null;
    }

    /**
     * 根据字典名称和字典中的值获取对应的名称
     *
     * @param name
     * @param val
     */
    @Override
    public String getDictsByName(String name, Integer val) {
        return null;
    }

    /**
     * 获取性别名称
     *
     * @param sex
     */
    @Override
    public String getSexName(Integer sex) {
        return null;
    }

    /**
     * 获取用户登录状态
     *
     * @param status
     */
    @Override
    public String getStatusName(Integer status) {
        return null;
    }

    /**
     * 获取菜单状态
     *
     * @param status
     */
    @Override
    public String getMenuStatusName(Integer status) {
        return null;
    }

    /**
     * 查询字典
     *
     * @param id
     */
    @Override
    public List<Dict> findInDict(Integer id) {
        return null;
    }

    /**
     * 获取被缓存的对象(用户删除业务)
     *
     * @param para
     */
    @Override
    public String getCacheObject(String para) {
        return null;
    }

    /**
     * 获取子部门id
     *
     * @param deptid
     */
    @Override
    public List<Integer> getSubDeptId(Integer deptid) {
        return null;
    }

    /**
     * 获取所有父部门id
     *
     * @param deptid
     */
    @Override
    public List<Integer> getParentDeptIds(Integer deptid) {
        return null;
    }
}
