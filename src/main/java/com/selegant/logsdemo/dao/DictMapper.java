package com.selegant.logsdemo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.selegant.logsdemo.model.Dict;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author stylefeng
 * @since 2017-07-11
 */
public interface DictMapper extends BaseMapper<Dict> {

    /**
     * 根据编码获取词典列表
     */
    List<Dict> selectByCode(@Param("pid") Integer pid);


    /**
     * 查询子对象
     */
    List<Dict> selectByCodeAll(@Param("pid") Integer pid);

    /**
     * 查询字典列表
     */
    List<Dict> list(@Param("condition") String condition);

    /**
     * 分页查询
     * @param condition
     * @return
     */
    List<Dict> list(@Param("page") Page<Dict> page, @Param("condition") String condition);
}